import datetime

def calculate_age(date):
    today = datetime.date.today()
    age = today.year - date.year - ((today.month, today.day) < (date.month, date.day))
    return age