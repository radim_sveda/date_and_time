import datetime

def second_from_midnight(time):
    # Pokusíme se převést řetězec na objekt datetime.time
    try:
        time_obj = datetime.datetime.strptime(time, "%H:%M:%S").time()
    except ValueError:
        raise ValueError("Neplatný formát vstupu")

    # Vypočítáme počet sekund od půlnoci
    return time_obj.hour * 3600 + time_obj.minute * 60 + time_obj.second